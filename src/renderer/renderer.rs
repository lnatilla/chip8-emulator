extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::rect::Rect;
// use sdl2::{Sdl, VideoSubsystem, EventPump};
use sdl2::EventPump;
use sdl2::render::WindowCanvas;

pub struct Renderer {
    // context: Sdl,
    // video_subsystem: VideoSubsystem,

    // window_width: u32,
    // window_height: u32,
    pixel_dimension: u16,

    canvas: WindowCanvas,

    pub event_pump: EventPump
}


impl Renderer {
    pub fn new(title: &str, pixel_dimension: u16) -> Self {
        let context = sdl2::init().unwrap();
        let video_subsystem = context.video().unwrap();

        let window_width = 64 * u32::from(pixel_dimension);
        let window_height = 32 * u32::from(pixel_dimension);

        let window = video_subsystem.window(title, window_width, window_height).position_centered().build().unwrap();
        let canvas = window.into_canvas().build().unwrap();

        let event_pump = context.event_pump().unwrap();

        return Renderer {
            // context,
            // video_subsystem,
            // window_width,
            // window_height,
            pixel_dimension,
            canvas,
            event_pump,
        }

    }

    pub fn draw(&mut self, x: u16, y: u16, ) {
       let sprite = Rect::new(i32::from(x * self.pixel_dimension),
       i32::from(y * self.pixel_dimension),
       u32::from(1 * self.pixel_dimension),
       u32::from(1 * self.pixel_dimension));


       match self.canvas.fill_rect(sprite) {
           Ok(result) => {
               return result
           }

           Err(error) => panic!("{}", error),
       }

    }

    pub fn clear(&mut self) {
        self.canvas.clear();
        self.canvas.present();
    }

    pub fn present(&mut self) {
        self.canvas.present();
    }


    pub fn set_color(&mut self, color: Color) {
        self.canvas.set_draw_color(color);
    }

    // pub fn get_color(&self, x: u16, y: u16) {
    // }
}
