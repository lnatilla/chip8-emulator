extern crate gl;

use gl::types::{GLsizeiptr, GLvoid};
#[derive(Debug)]
pub enum ShaderDataType {
    None = 0,
    Bool = 1,
    Float = 2,
    Float2 = 3,
    Float3 = 4,
    Float4 = 5,
    Int = 6,
    Int2 = 7,
    Int3 = 8,
    Int4 = 9,
    Mat3 = 10,
    Mat4 = 11,
}

fn shader_datatype_size(shader_type: ShaderDataType) -> u32 {
    match shader_type {
        ShaderDataType::Bool => 1,
        ShaderDataType::Float => 4,
        ShaderDataType::Float2 => 4 * 2,
        ShaderDataType::Float3 => 4 * 3,
        ShaderDataType::Float4 => 4 * 4,
        ShaderDataType::Int => 4,
        ShaderDataType::Int2 => 4 * 2,
        ShaderDataType::Int3 => 4 * 3,
        ShaderDataType::Int4 => 4 * 4,
        ShaderDataType::Mat3 => 4 * 3 * 3,
        ShaderDataType::Mat4 => 4 * 4 * 4,

        _ => {
            println!("ShaderDataType not handled! {:?}", shader_type);
            0
        }
    }
}

fn shader_datatype_to_gl_type(shader_type: ShaderDataType) -> u32 {
    match shader_type {
        ShaderDataType::Bool => 1,
        ShaderDataType::Float => 32,
        ShaderDataType::Float2 => 32,
        ShaderDataType::Float3 => 32,
        ShaderDataType::Float4 => 32,
        ShaderDataType::Int => 32,
        ShaderDataType::Int2 => 32,
        ShaderDataType::Int3 => 32,
        ShaderDataType::Int4 => 32,
        ShaderDataType::Mat3 => 32,
        ShaderDataType::Mat4 => 32,

        _ => {
            println!("ShaderDataType not handled! {:?}", shader_type);
            0
        }
    }
}

pub struct VertexBufferElement {
    pub name: String,
    pub shader_type: ShaderDataType,
    pub size: u32,
    pub offset: u32,
    pub normalized: bool,
}

impl VertexBufferElement {
    pub fn new(shader_type: ShaderDataType, name: String, normalized: Option<bool>) -> Self {
        match normalized {
            Some(result) => VertexBufferElement {
                name,
                shader_type,
                size: shader_datatype_size(shader_type),
                offset: 0,
                normalized: result,
            },
            None => VertexBufferElement {
                name,
                shader_type,
                size: shader_datatype_size(shader_type),
                offset: 0,
                normalized: false,
            },
        }
    }

    pub fn component_count(&self) -> u32 {
        match self.shader_type {
            ShaderDataType::Bool => 1,
            ShaderDataType::Float => 1,
            ShaderDataType::Float2 => 2,
            ShaderDataType::Float3 => 3,
            ShaderDataType::Float4 => 4,
            ShaderDataType::Int => 1,
            ShaderDataType::Int2 => 2,
            ShaderDataType::Int3 => 3,
            ShaderDataType::Int4 => 4,
            ShaderDataType::Mat3 => 3,
            ShaderDataType::Mat4 => 4,

            _ => {
                println!("ShaderDataType not handled! {:?}", self.shader_type);
                0
            }
        }
    }
}

pub struct VertexBuffer {
    vbo: u32,
}

impl VertexBuffer {
    pub fn new<T>(data: Vec<T>) -> Self {
        unsafe {
            gl::GenBuffers(1, &mut 0);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BufferData(
                0,
                (data.len() * std::mem::size_of::<T>()) as GLsizeiptr,
                data.as_ptr() as *const GLvoid,
                gl::STATIC_DRAW,
            );
        }
        VertexBuffer { vbo: 0 }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }
    }

    pub fn set_data<T>(&self, data: Vec<T>) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferSubData(
                gl::ARRAY_BUFFER,
                0,
                (data.len() * std::mem::size_of::<T>()) as GLsizeiptr,
                data.as_ptr() as *const GLvoid,
            );
        }
    }
}

impl Drop for VertexBuffer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}
