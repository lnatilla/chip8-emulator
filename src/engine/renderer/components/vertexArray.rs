extern crate gl;

use crate::engine::renderer::components::vertexBuffer::VertexBuffer;

pub struct VertexArray {
    vao: u32,
    vbo_index: u32,
    vbo: Option<VertexBuffer>,
}

impl VertexArray {
    pub fn new() -> Self {
        unsafe {
            gl::GenVertexArrays(1, &mut 0);
        }
        VertexArray {
            vao: 0,
            vbo_index: 0,
            vbo: None,
        }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.vao);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for VertexArray {
    pub fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
        }
    }
}
