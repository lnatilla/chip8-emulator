extern crate gl;
extern crate glfw;
extern crate imgui;

mod engine;

use engine::renderer::*;
use gl::types::{GLfloat, GLsizeiptr};
use glfw::{Action, Context, Key};

fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    let (mut window, events) = glfw
        .create_window(800, 800, "Test", glfw::WindowMode::Windowed)
        .expect("Failed to create window!");

    window.make_current();
    window.set_key_polling(true);

    gl::load_with(|s| glfw.get_proc_address_raw(s));

    let mut vertex_array_buffer = 0;
    let mut vertex_buffer_object = 0;

    let vertices: Vec<GLfloat> = vec![-0.25, -0.25, 0.0, -0.25, 0.25, 0.0, 0.25, 0.25, 0.0];

    unsafe {
        gl::Viewport(0, 0, 800, 800);
        gl::GenVertexArrays(1, &mut vertex_array_buffer);
        gl::GenBuffers(1, &mut vertex_buffer_object);

        gl::BindVertexArray(vertex_array_buffer);
        gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer_object);

        gl::BufferData(
            gl::ARRAY_BUFFER,
            (vertices.len() * std::mem::size_of::<GLfloat>()) as GLsizeiptr,
            std::mem::transmute(&vertices[0]),
            gl::STATIC_DRAW,
        );
        gl::VertexAttribPointer(
            0,
            3,
            32,
            gl::FALSE,
            (std::mem::size_of::<f64>() * 3) as i32,
            std::ptr::null(),
        );

        gl::BindVertexArray(vertex_array_buffer);
        gl::DrawArrays(gl::TRIANGLES, 0, 3);

        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        gl::BindVertexArray(0);
    }

    while !window.should_close() {
        window.swap_buffers();

        unsafe {
            gl::ClearColor(1.0, 1.0, 1.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }

        glfw.poll_events();
        for (_, event) in glfw::flush_messages(&events) {
            match event {
                glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                    window.set_should_close(true);
                }
                _ => {}
            }
        }
    }
}
