use std::fmt::Display;

#[derive(Clone)]
pub struct Instruction {
    pub code: u16,
    pub first_byte: u16,
    pub x: u16,
    pub y: u16,
    pub last_byte: u16
}


impl Instruction {
    pub fn new(code: u16) -> Self {
        let mut instruction = Instruction {first_byte: 0, code, x: 0, y: 0, last_byte: 0};

        instruction.first_byte = instruction.code & 0xF000;
        instruction.x = instruction.code & 0x0F00;
        instruction.y = instruction.code & 0x00F0;
        instruction.last_byte = instruction.code & 0x000F;

        instruction
    }

    pub fn from(first: u8, second: u8) -> Self {
       Instruction::new(
           u16::from(first) << 8 |
           u16::from(second)
       ) 
    }
}

impl Display for Instruction {
    fn fmt (&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
       write!(fmt, "Instruction: {:#04X?}\n
           - First Byte: {:#02X?}\n
           - X: {:#02X?}\n - Y: {:#02X?}\n
           - Last Byte: {:#02X?}\n",
           self.code, self.first_byte, self.x, self.y, self.last_byte)
    }
}
