use std::fmt::Display;

#[allow(non_camel_case_types, dead_code)]
#[derive(Debug, Copy, Clone)]
pub enum Register {
    V0 = 0,
    V1 = 1,
    V2 = 2,
    V3 = 3,
    V4 = 4,
    V5 = 5,
    V6 = 6,
    V7 = 7,
    V8 = 8,
    V9 = 9,
    VA = 10,
    VB = 11,
    VC = 12,
    VD = 13,
    VE = 14,
    VF = 15
}

#[derive(Debug, Default, Clone)]
pub struct RegisterBank(pub [u8; 16]); 

impl std::ops::Index<Register> for RegisterBank {
    type Output = u8;
    fn index(&self, index: Register) -> &Self::Output {     
        &self.0[index as usize] 
    }
}

impl std::ops::IndexMut<Register> for RegisterBank {
    fn index_mut(&mut self, index: Register) -> &mut Self::Output {     
        &mut self.0[index as usize] 
    }
}
