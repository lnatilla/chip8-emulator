use crate::util;
use crate::renderer::renderer::Renderer;
use crate::system::components::instruction;

extern crate sdl2;

use sdl2::pixels::Color;

pub struct Chip8 {

    // This is where we will store op codes
    op_code: u16,
    // Which op code to execute
    program_counter: usize,

    // Memory
    memory: [u8; 4096],
    memory_start: usize,

    // Registers
    v: [u16; 16],
    // Particular register used to make operations with memory
    address_register: u16,


    //Graphics buffer
    // gfx: [u8; 2048],


    //Interrupt and hardware registers
    // delay_timer: u8,
    // sound_timer: u8,


    //Stack
    stack: [u16; 16],
    //Stack Pointer
    stack_pointer: usize,

    //State of each key (Numpad 0x0, 0xF)
    // keys: [u8; 16],


    fontset: [u8; 80],
    rom_path: String,

    renderer: Renderer

}

impl Chip8 {

    pub fn new(renderer: Renderer) -> Chip8 {
        return Chip8 {
            op_code: 0,
            program_counter: 0,
            memory: [0; 4096],
            memory_start: 0x200,
            address_register: 0,
            // delay_timer: 0,
            fontset: [0; 80],
            // gfx: [0; 2048],
            // keys: [0; 16], 
            stack: [0; 16], 
            stack_pointer: 0,
            // sound_timer: 0,
            v: [0; 16],
            rom_path: "roms/test_rom.ch8".to_string(),
            renderer,
        }
    }

    pub fn initialize(&mut self) {
        // The program starts at this memory address as before there are
        // store font data and 96 bytes below the top of the memory are
        // reserved for the call stack.
        self.memory_start = 0x200;


        self.fontset = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        ];

        // Set everything to zero
        self.program_counter = self.memory_start;
        self.op_code = 0;
        self.address_register = 0;
        self.stack_pointer = 0;

        // Load fontset in the memory reserved for it (below 0x200)
        self.load_fontset();

        // Load program in memory from 0x200
        self.load_program();
    }


    pub fn emulate_cycle(&mut self) {
       // Fetch opcode
        self.fetch_op_code();

        // self.op_code & 0xF000 only takes the first 4 bits which explain which opcode 
        // needs to be executed
   
        util::print_hex_label("Full Op Code:", self.op_code, 4);

        // Decode Op Code
        let (op_code, value) = self.decode_op_code();
        let (x, y) = self.get_x_y();

        util::print_hex_label("Op Code:", op_code, 4);
        util::print_hex_label("Value:", value, 3);

        util::print_hex_label("X:", x, 1);
        util::print_hex_label("Y:", y, 1);

        // Execute Op Code
        match op_code {


            // self.op_code & 0x000F stores 12 bits from right into address register
            0xA000 => {
                self.address_register = value;
                self.program_counter += 2;
            }

            0x1000 => {
                self.program_counter = usize::from(value);
            }

            0x2000 => {
                self.stack[self.stack_pointer] = self.program_counter as u16;
                self.stack_pointer += 1;
                self.program_counter = usize::from(value);     
            }

            0x6000 => {
                self.v[x as usize] = self.last_two(value);
                self.program_counter += 2;
            }

            0x0000 => {
                match self.op_code & 0x000F {
                    // Clear screen
                    0x00E0 => {
                        self.renderer.clear(); 
                    }

                    // Return from subroutine
                    0x00EE => {
                        self.program_counter = usize::from(self.stack[self.stack.len() - 1]);
                    }

                    _ => {
                        println!("Unhandled {:x?}", op_code);
                    }
                }
            }

            0xD000 => {
                let height: u16 = value & 0x00F;

                self.renderer.set_color(Color::RGB(255, 255, 255));

                for i in 0..height + 1 {
                    let mut sprite = self.memory[(self.address_register + i) as usize];

                    for j in 0..8 { 
                        if (sprite & 0x80) > 0 {
                            self.renderer.draw(self.v[(x + i) as usize], self.v[(y + j) as usize]);
                           // self.renderer.get_color(self.v[x as usize], self.v[y as usize]);
                        }
                        sprite <<= 1;
                    }
                }

                self.renderer.present();
            }

            0xE000 => {
                match self.op_code & 0x00FF {
                    0xA1 => {
                        if (self.v[x as usize]& 0x0F00) << 8 != 0 {
                            self.program_counter += 4
                        }
                    }
                    
                    _ => {
                        println!("Unhandled {:x?}", op_code);
                    }
                }
            }

            _ => {
                println!("Unhandled {:x?}", op_code);
            }
        }

    }

    fn load_fontset(&mut self) {
        for i in 0..80 {
            self.memory[i] = self.fontset[i]; 
        }
    }

    fn load_program(&mut self) {
        let program_binary = util::read_file_binary(&self.rom_path);
        
        for i in 0..program_binary.len() {
            self.memory[self.memory_start + i] = program_binary[i];
        }

    }

    fn fetch_op_code(&mut self) {
       // Opcode is formed by memory[pc] << 8 | memory[pc + 1]
        self.op_code = u16::from(self.memory[self.program_counter]) << 8 | u16::from(self.memory[self.program_counter + 1]);
    }

    fn decode_op_code(&self) -> (u16, u16) {
        return (self.op_code & 0xF000, self.op_code & 0x0FFF)
    }

    fn get_x_y(&self) -> (u16, u16) {
        return ((self.op_code & 0x0F00) >> 8, (self.op_code & 0x00F0) >> 4)
    }

    fn last_two(&self, value: u16) -> u16 {
        return value & 0x00FF
    }

}
