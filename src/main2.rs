mod system;
mod util;
mod renderer;

use std::io::stdin;

extern crate sdl2;
extern crate imgui;
extern crate imgui_sdl2;
extern crate gl;
extern crate imgui_opengl_renderer;


fn main() {
    println!("Welcome from chip8-emulator!");    
    let renderer = renderer::renderer::Renderer::new("Chip8 Emulator", 30);

    let mut chip8 = system::chip8::Chip8::new(renderer);
    chip8.initialize();


    for _ in 0..10 {

        chip8.emulate_cycle();
        let mut s = String::new();
        stdin().read_line(&mut s).expect("error!");
        // for event in renderer.event_pump.poll_iter() { 
            // match event {
                // Event::Quit {..} |
                // Event::KeyDown {keycode: Some(Keycode::Escape), .. } => {
                    // break 'running
                // }

                // _ => {}
            // }
        // }
    }
}

