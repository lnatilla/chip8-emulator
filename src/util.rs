pub fn read_file_binary(filename: &String) -> Vec<u8> {
    match std::fs::read(filename) {
        Ok(bytes) => {
            return bytes
        }

        Err(error) => {
            panic!("{}", error);
        }
    }
}

// pub fn print_hex<T: std::fmt::Debug>(value: T, len: usize){
    // println!("{:01$X?}", value, len);
// }

pub fn print_hex_label<T: std::fmt::Debug>(message: &str, value: T, len: usize) {
    println!("{} {:02$X?}", message, value, len);
}
